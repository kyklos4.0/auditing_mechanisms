#!/bin/bash

#
# [docker-compose.sh]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#



echo "removing old containers"
docker rm -f server_kafka_1
docker rm -f server_zookeeper_1
docker rm -f server_mongodb_1
docker rm -f server_eventeum_1
docker rm -f server_parity_1
docker rm -f server_logstash_1
docker rm -f server_elasticsearch_1
docker-compose down

echo "removing storage"
sudo rm -rf $HOME/mongodb/data
sudo rm -rf $HOME/parity/data:/root/
sudo rm -rf $HOME/parity/log

composescript="docker-compose.yml"

if [ "$1" = "rinkeby" ]; then
   composescript="docker-compose-rinkeby.yml"
   echo "Running in Rinkeby Infura mode..."
elif [ "$1" = "infra" ]; then
   composescript="docker-compose-infra.yml"
   echo "Running in Infrastructure mode..."
fi

docker-compose -f "$composescript" build
[ $? -eq 0 ] || exit $?;


echo "Start"
docker-compose -f "$composescript" up
[ $? -eq 0 ] || exit $?;

trap "docker-compose -f "$composescript" kill" INT
