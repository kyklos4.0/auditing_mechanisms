#!/bin/sh

#
# [sus.sh]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#

if [ $# -eq 1 ]; then
	kyklos_contract_address="$1"
else
	echo "Error en los argumentos. La ejecución requiere como único argumento la dirección del KYKLOS.sol"
	exit
fi

echo "Contract address: " $kyklos_contract_address


# New Admin
event_definition="{\"id\": \"newAdminEvent_$kyklos_contract_address\",
\"contractAddress\": \"$kyklos_contract_address\",
\"eventSpecification\": {
  \"eventName\": \"newAdminEvent\",
  \"nonIndexedParameterDefinitions\": [
    {\"position\": 0, \"type\": \"ADDRESS\"},
    {\"position\": 1, \"type\": \"ADDRESS\"},
    {\"position\": 2, \"type\": \"STRING\"}] },
	\"correlationIdStrategy\": {
	  \"type\": \"NON_INDEXED_PARAMETER\",
	  \"parameterIndex\":0}
}"

curl -X POST http://10.5.0.40:8060/api/rest/v1/event-filter \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 616712a3-bf11-bbf5-b4ac-b82835779d51' \
-d "$event_definition"

# Deleted Admin
event_definition="{\"id\": \"removeAdminEvent_$kyklos_contract_address\",
\"contractAddress\": \"$kyklos_contract_address\",
\"eventSpecification\": {
  \"eventName\": \"removeAdminEvent\",
  \"nonIndexedParameterDefinitions\": [
    {\"position\": 0, \"type\": \"ADDRESS\"},
    {\"position\": 1, \"type\": \"STRING\"}] },
	\"correlationIdStrategy\": {
	  \"type\": \"NON_INDEXED_PARAMETER\",
	  \"parameterIndex\":0}
}"

curl -X POST http://10.5.0.40:8060/api/rest/v1/event-filter \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 616712a3-bf11-bbf5-b4ac-b82835779d51' \
-d "$event_definition"

# New Component
event_definition="{\"id\": \"newComponentEvent_$kyklos_contract_address\",
\"contractAddress\": \"$kyklos_contract_address\",
\"eventSpecification\": {
  \"eventName\": \"newComponentEvent\",
  \"nonIndexedParameterDefinitions\": [
    {\"position\": 0, \"type\": \"ADDRESS\"},
    {\"position\": 1, \"type\": \"ADDRESS\"},
    {\"position\": 2, \"type\": \"STRING\"},
    {\"position\": 3, \"type\": \"STRING\"},
    {\"position\": 4, \"type\": \"STRING\"},
    {\"position\": 5, \"type\": \"STRING\"},
    {\"position\": 6, \"type\": \"UINT256\"},
    {\"position\": 7, \"type\": \"UINT256\"}] },
	\"correlationIdStrategy\": {
	  \"type\": \"NON_INDEXED_PARAMETER\",
	  \"parameterIndex\":0}
}"

curl -X POST http://10.5.0.40:8060/api/rest/v1/event-filter \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 616712a3-bf11-bbf5-b4ac-b82835779d51' \
-d "$event_definition"

