#!/bin/bash

#
# [send_email_component.sh]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#

email_list="$1"
component_id="$2"
variable="$3"
value="$4"

echo email_list

#Get from ElasticSearch
case $variable in
    name)
        previous_value=$(curl --user elastic:elastic -X GET http://10.5.0.10:9200/kyklos_components/_search?q=componentid:"$component_id" | jq .hits.hits[0]._source.name | tr -d '"')
        new_value="$value"
        ;;
    organization)
        previous_value=$(curl --user elastic:elastic -X GET http://10.5.0.10:9200/kyklos_components/_search?q=componentid:"$component_id" | jq .hits.hits[0]._source.organization | tr -d '"')
        new_value="$value"
        ;;
    deployment)
        previous_value=$(curl --user elastic:elastic -X GET http://10.5.0.10:9200/kyklos_components/_search?q=componentid:"$component_id" | jq .hits.hits[0]._source.deployment | tr -d '"')
        new_value="$value"
        ;;
    description)
        previous_value=$(curl --user elastic:elastic -X GET http://10.5.0.10:9200/kyklos_components/_search?q=componentid:"$component_id" | jq .hits.hits[0]._source.description | tr -d '"')
        new_value="$value"
        ;;
esac

#Send Email
curl --request POST --url https://api.sendgrid.com/v3/mail/send --header 'Authorization: Bearer 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' --header 'Content-Type: application/json' --data "{\"personalizations\": [{\"to\": [{\"email\": \"$email_list\"}]}],\"from\": {\"email\": \"trustworthykyklos@gmail.com\"},\"subject\": \"KYKLOS Update\",\"content\": [{\"type\": \"text/plain\", \"value\": \"Variable: $variable; Previous value: $previous_value; Current value: $new_value\"}]}"


