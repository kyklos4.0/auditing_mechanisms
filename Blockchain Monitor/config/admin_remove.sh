#!/bin/bash

#
# [logstash.yml]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#

admin_address="$1"
rol="$2"
#Remove from ElasticSearch
id=$(curl --user elastic:elastic -X GET http://10.5.0.10:9200/kyklos_admins/_search?q=adminaddress:"$admin_address"%20AND%20rol:"$rol" | jq .hits.hits[0]._id | tr -d '"')
curl --user elastic:elastic -XDELETE http://10.5.0.10:9200/kyklos_admins/_doc/$id | tr -d '\'
