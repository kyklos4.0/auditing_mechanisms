#!/bin/sh

#
# [component_sus.sh]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#

component_contract_address="$1"

# Updated component
event_definition="{\"id\": \"ComponentUpdated_$component_contract_address\",
\"contractAddress\": \"$component_contract_address\",
\"eventSpecification\": {
  \"eventName\": \"ComponentUpdated\",
  \"nonIndexedParameterDefinitions\": [
    {\"position\": 0, \"type\": \"ADDRESS\"},
    {\"position\": 1, \"type\": \"ADDRESS\"},
    {\"position\": 2, \"type\": \"STRING\"},
    {\"position\": 3, \"type\": \"STRING\"},
    {\"position\": 4, \"type\": \"UINT256\"},
    {\"position\": 5, \"type\": \"STRING\"}] },
        \"correlationIdStrategy\": {
          \"type\": \"NON_INDEXED_PARAMETER\",
          \"parameterIndex\":0}
}"

curl -X POST http://10.5.0.40:8060/api/rest/v1/event-filter \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 616712a3-bf11-bbf5-b4ac-b82835779d51' \
-d "$event_definition"



# New Component Features
event_definition="{\"id\": \"NewComponentFeature_$component_contract_address\",
\"contractAddress\": \"$component_contract_address\",
\"eventSpecification\": {
  \"eventName\": \"NewComponentFeature\",
  \"nonIndexedParameterDefinitions\": [
    {\"position\": 0, \"type\": \"ADDRESS\"},
    {\"position\": 1, \"type\": \"ADDRESS\"},
    {\"position\": 2, \"type\": \"STRING\"},
    {\"position\": 3, \"type\": \"STRING\"},
    {\"position\": 4, \"type\": \"UINT256\"},
    {\"position\": 5, \"type\": \"STRING\"}] },
        \"correlationIdStrategy\": {
          \"type\": \"NON_INDEXED_PARAMETER\",
          \"parameterIndex\":0}
}"

curl -X POST http://10.5.0.40:8060/api/rest/v1/event-filter \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 616712a3-bf11-bbf5-b4ac-b82835779d51' \
-d "$event_definition"

# New NewValueFeature
event_definition="{\"id\": \"NewValueFeature_$component_contract_address\",
\"contractAddress\": \"$component_contract_address\",
\"eventSpecification\": {
  \"eventName\": \"NewValueFeature\",
  \"nonIndexedParameterDefinitions\": [
    {\"position\": 0, \"type\": \"ADDRESS\"},
    {\"position\": 1, \"type\": \"ADDRESS\"},
    {\"position\": 2, \"type\": \"STRING\"},
    {\"position\": 3, \"type\": \"STRING\"},
    {\"position\": 4, \"type\": \"UINT256\"},
    {\"position\": 5, \"type\": \"STRING\"}] },
        \"correlationIdStrategy\": {
          \"type\": \"NON_INDEXED_PARAMETER\",
          \"parameterIndex\":0}
}"

curl -X POST http://10.5.0.40:8060/api/rest/v1/event-filter \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 616712a3-bf11-bbf5-b4ac-b82835779d51' \
-d "$event_definition"
