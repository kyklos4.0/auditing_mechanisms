#!/bin/sh

#
# [configure_logstash.sh]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#


yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install jq -y
logstash -f /usr/share/logstash/config/logstash.conf
