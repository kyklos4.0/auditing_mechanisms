#!/bin/bash

#
# [feature_update.sh]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#

component_id="$1"
variable="$2"
value="$3"
timevalue="$4"

#Update in ElasticSearch
id=$(curl --user elastic:elastic -X GET http://10.5.0.10:9200/kyklos_features/_search?q=variable:"$variable"%20AND%20componentid:"$component_id" | jq .hits.hits[0]._id | tr -d '"')





curl --user elastic:elastic -X POST http://10.5.0.10:9200/kyklos_features/_update/$id -H 'Content-Type: application/json' -d '{"doc":{"value":"'$value'"}}'
curl --user elastic:elastic -X POST http://10.5.0.10:9200/kyklos_features/_update/$id -H 'Content-Type: application/json' -d '{"doc":{"updatedTimestamp":"'$timevalue'"}}'
