# KYKLOS 4.0 Auditing Mechanisms

This project contains the solution of the KYKLOS 4.0 Auditing Mechanisms

## Getting Started

### Smart Contracts
The KYKLOS.sol Smart Contract must be deployed over a Ethereum-like Blockchain network. In KYKLOS 4.0, a test Quorum network was considered. 
It is important to identify the Blockchain address of the deployed Smart Contract (bc_address).

### Blockchain Monitor
The Blockchain Monitor is automatically deployed executing ./Blockchain Monitor/docker-compose.sh.
Once the different services from the Blockchain Monitor are running, suscription to the events of the previously deployed Smart Contract is needed through ./Blocjcain Monitor/sus.sh "bc_address"

### Administrator client
The docker image of the administrator client is generated through ./Administrator client/docker_build.sh.
A docker image will be locally generated. It can be run through docker run -d -p 8001:8001 kyklos_blockchain_client:latest

### Component client
The docker image of the component client is generated through ./Component client/docker_build.sh.
A docker image will be locally generated. It can be run through docker run -d -p 8001:8001 kyklos_blockchain_client_component:latest
