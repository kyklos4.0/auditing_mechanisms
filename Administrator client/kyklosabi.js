//
// [kyklosabi.js]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//

const mainabi=[
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_newAdmin",
                                "type": "address"
                        }
                ],
                "name": "addAdmin",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_newUser",
                                "type": "address"
                        }
                ],
                "name": "addAuthorizedUser",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_deployment",
                                "type": "string"
                        },
                        {
                                "name": "_organization",
                                "type": "string"
                        },
                        {
                                "name": "_name",
                                "type": "string"
                        },
                        {
                                "name": "_description",
                                "type": "string"
                        }
                ],
                "name": "createComponent",
                "outputs": [
                        {
                                "name": "",
                                "type": "address"
                        }
                ],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_admin",
                                "type": "address"
                        }
                ],
                "name": "removeAdmin",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_user",
                                "type": "address"
                        }
                ],
                "name": "removeAuthorizedUser",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "inputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "constructor"
        },
        {
                "anonymous": false,
                "inputs": [
                        {
                                "indexed": false,
                                "name": "newAdmin",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "addedBy",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "rol",
                                "type": "string"
                        }
                ],
                "name": "newAdminEvent",
                "type": "event"
        },
        {
                "anonymous": false,
                "inputs": [
                        {
                                "indexed": false,
                                "name": "admin",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "rol",
                                "type": "string"
                        }
                ],
                "name": "removeAdminEvent",
                "type": "event"
        },
        {
                "anonymous": false,
                "inputs": [
                        {
                                "indexed": false,
                                "name": "component",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "owner",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "organization",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "name",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "description",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "deployment",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "creationTimestamp",
                                "type": "uint256"
                        },
                        {
                                "indexed": false,
                                "name": "updatedTimestamp",
                                "type": "uint256"
                        }
                ],
                "name": "newComponentEvent",
                "type": "event"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getAdminNum",
                "outputs": [
                        {
                                "name": "",
                                "type": "uint256"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getAuthorizedNum",
                "outputs": [
                        {
                                "name": "",
                                "type": "uint256"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getcomponentbyowner",
                "outputs": [
                        {
                                "name": "",
                                "type": "address"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getComponents",
                "outputs": [
                        {
                                "name": "",
                                "type": "address[]"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getComponentsNum",
                "outputs": [
                        {
                                "name": "",
                                "type": "uint256"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [
                        {
                                "name": "_admin",
                                "type": "address"
                        }
                ],
                "name": "isAdmin",
                "outputs": [
                        {
                                "name": "",
                                "type": "bool"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [
                        {
                                "name": "_user",
                                "type": "address"
                        }
                ],
                "name": "isAuthorized",
                "outputs": [
                        {
                                "name": "",
                                "type": "bool"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [
                        {
                                "name": "",
                                "type": "address"
                        }
                ],
                "name": "validAdmin",
                "outputs": [
                        {
                                "name": "",
                                "type": "bool"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [
                        {
                                "name": "",
                                "type": "address"
                        }
                ],
                "name": "validComponents",
                "outputs": [
                        {
                                "name": "",
                                "type": "bool"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [
                        {
                                "name": "",
                                "type": "address"
                        }
                ],
                "name": "validUsers",
                "outputs": [
                        {
                                "name": "",
                                "type": "bool"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        }
];

const componentabi =
[
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_description",
                                "type": "string"
                        }
                ],
                "name": "setComponentDescription",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [
                        {
                                "name": "_variableName",
                                "type": "string"
                        }
                ],
                "name": "getFeatureByValue",
                "outputs": [
                        {
                                "name": "",
                                "type": "string"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getUpdatedTime",
                "outputs": [
                        {
                                "name": "",
                                "type": "uint256"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getName",
                "outputs": [
                        {
                                "name": "",
                                "type": "string"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getDescription",
                "outputs": [
                        {
                                "name": "",
                                "type": "string"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_deployment",
                                "type": "string"
                        }
                ],
                "name": "setComponentDeployment",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_variableName",
                                "type": "string"
                        },
                        {
                                "name": "_variableValue",
                                "type": "string"
                        }
                ],
                "name": "setFeatureDescription",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [],
                "name": "deleteControlEmail",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_email",
                                "type": "string"
                        }
                ],
                "name": "addControlEmail",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getId",
                "outputs": [
                        {
                                "name": "",
                                "type": "address"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getEmail",
                "outputs": [
                        {
                                "name": "",
                                "type": "string"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getDeployment",
                "outputs": [
                        {
                                "name": "",
                                "type": "string"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getOwner",
                "outputs": [
                        {
                                "name": "",
                                "type": "address"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_name",
                                "type": "string"
                        }
                ],
                "name": "setComponentName",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": false,
                "inputs": [
                        {
                                "name": "_organization",
                                "type": "string"
                        }
                ],
                "name": "setComponentOrganization",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getOrganization",
                "outputs": [
                        {
                                "name": "",
                                "type": "string"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getFeatureList",
                "outputs": [
                        {
                                "name": "",
                                "type": "string[]"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "constant": true,
                "inputs": [],
                "name": "getCreationTime",
                "outputs": [
                        {
                                "name": "",
                                "type": "uint256"
                        }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
        },
        {
                "inputs": [
                        {
                                "name": "_owner",
                                "type": "address"
                        },
                        {
                                "name": "_organization",
                                "type": "string"
                        },
                        {
                                "name": "_name",
                                "type": "string"
                        },
                        {
                                "name": "_description",
                                "type": "string"
                        },
                        {
                                "name": "_deployment",
                                "type": "string"
                        },
                        {
                                "name": "_creationTimestamp",
                                "type": "uint256"
                        }
                ],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "constructor"
        },
        {
                "anonymous": false,
                "inputs": [
                        {
                                "indexed": false,
                                "name": "id",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "owner",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "variable",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "value",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "updatedTimestamp",
                                "type": "uint256"
                        },
                        {
                                "indexed": false,
                                "name": "email",
                                "type": "string"
                        }
                ],
                "name": "ComponentUpdated",
                "type": "event"
        },
        {
                "anonymous": false,
                "inputs": [
                        {
                                "indexed": false,
                                "name": "id",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "owner",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "variableName",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "variableValue",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "updatedTimestamp",
                                "type": "uint256"
                        },
                        {
                                "indexed": false,
                                "name": "email",
                                "type": "string"
                        }
                ],
                "name": "NewComponentFeature",
                "type": "event"
        },
        {
                "anonymous": false,
                "inputs": [
                        {
                                "indexed": false,
                                "name": "id",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "owner",
                                "type": "address"
                        },
                        {
                                "indexed": false,
                                "name": "variableName",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "variableValue",
                                "type": "string"
                        },
                        {
                                "indexed": false,
                                "name": "updatedTimestamp",
                                "type": "uint256"
                        },
                        {
                                "indexed": false,
                                "name": "email",
                                "type": "string"
                        }
                ],
                "name": "NewValueFeature",
                "type": "event"
        }
];

module.exports = {
        "mainabi": mainabi,
        "componentabi": componentabi
};
