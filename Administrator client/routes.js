//
// [routes.js]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//


var express = require('express');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
var http = require('http');
var functionality = require('./kyklos_bc.js');
var app = express();
const fs = require('fs');
const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};


app.get('/client/admin', (req, res) => {
        functionality.isadmin(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/admin', (req, res) => {
        functionality.addadmin(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.delete('/client/admin', (req, res) => {
        functionality.deleteadmin(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/admins', (req, res) => {
        functionality.getadminnum().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/componentsnum', (req, res) => {
        functionality.getcomponentsnum().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/components', (req, res) => {
        functionality.getcomponents().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/authorizeduser', (req, res) => {
        functionality.isauthorized(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/authorizeduser', (req, res) => {
        functionality.addauthorized(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.delete('/client/authorizeduser', (req, res) => {
        functionality.deleteauthorized(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/authorizedusers', (req, res) => {
        functionality.getauthorizednum().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

http.createServer(options, app).listen(8001, () => {
        console.log("KYKLOS Client HTTP Server Started in TECNALIA..."); })
app.use('/doc',swaggerUi.serve, swaggerUi.setup(swaggerFile));
