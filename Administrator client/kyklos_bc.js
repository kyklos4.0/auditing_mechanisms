//
// [kyklos_bc.js]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//

const Web3 = require('web3')
const web3 = new Web3(new Web3.providers.HttpProvider('http://172.26.41.212:22000'));
var kyklosabi = require('./kyklosabi.js');
const kyklosaddress = '0x1932c48b2bF8102Ba33B4A6B545C32236e342f34';
const kykloscontract = new web3.eth.Contract(kyklosabi.mainabi, kyklosaddress, {gasPrice:'0'});
var num_component = 0;
var kykloscomponentaddress ;
const index = 0;
const estimatedgas = 8000000;
var myaddress= '0xed9d02e382b34818e88B88a309c7fe71E65f419d';
var kykloscomponentcontract;
web3.eth.handleRevert = true;

async function addadmin(adminaddress) {
        if(adminaddress == "" || typeof adminaddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Admin Address parameter is not properly defined" }';
                console.log("addadmin: Error: Admin Address parameter is not properly defined");
                return[code,answer];
        }
        return await kykloscontract.methods.addAdmin(adminaddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(txRaw) {
                var answer = '{"status":"OK", "address":"'+adminaddress+'" }';
                var code =  200;
                console.log("addadmin: Administrator rights have been given");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error giving administrator rights" }';
                var code =  403;
                console.log("addadmin: Error:",error.reason);
                return[code,answer];
     })
}

async function deleteadmin(adminaddress) {
        if(adminaddress == "" || typeof adminaddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Admin Address parameter is not properly defined" }';
                console.log("deleteadmin: Error: Admin Address parameter is not properly defined");
                return[code,answer];
        }

        return await kykloscontract.methods.removeAdmin(adminaddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(txRaw) {
                var answer = '{"status":"OK", "address":"'+adminaddress+'" }';
                var code =  200;
                console.log("deleteadmin: Administrator rights have been removed");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error removing administrator rights" }';
                console.log("deleteadmin: Error:",error.reason);
                var code =  403;
                return[code,answer];

     })
}


async function isadmin(adminaddress) {
        if(adminaddress == "" || typeof adminaddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Admin Address parameter is not properly defined" }';
                console.log("isadmin: Error: Admin Address parameter is not properly defined");
                return[code,answer];
        }

        return await kykloscontract.methods.isAdmin(adminaddress).call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "isadmin":"'+ans+'" }';
                var code =  200;
                console.log("isadmin: Administrator rights have been checked");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error checking Administrator rights" }';
                var code =  403;
                console.log("isadmin: Error:",error.reason);
                return[code,answer];
     })
}

async function getadminnum() {
        return await kykloscontract.methods.getAdminNum().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "adminnum":"'+ans+'" }';
                var code =  200;
                console.log("getadminnum: The number of administrators have been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting admin num" }';
                var code =  403;
                console.log("getadminnum: Error:",error.reason);
                return[code,answer];
     })
}


async function getcomponents() {
        return await kykloscontract.methods.getComponents().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "componentid":"'+ans+'" }';
                var code =  200;
                console.log("getcomponents: The components ids have been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting components" }';
                var code =  403;
                console.log("getcomponents: Error:",error.reason);
                return[code,answer];
     })
}

async function getcomponentsnum() {
        return await kykloscontract.methods.getComponentsNum().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "componentssnum":"'+ans+'" }';
                var code =  200;
                console.log("getcomponentsnum: The number of components has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting components num" }';
                var code =  403;
                console.log("getcomponentsnum: Error:",error.reason);
                return[code,answer];
     })
}


async function addauthorized(useraddress) {
        if(useraddress == "" || typeof useraddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"User Address parameter is not properly defined" }';
                console.log("addauthorized: Error: User Address parameter is not properly defined");
                return[code,answer];
        }
        return await kykloscontract.methods.addAuthorizedUser(useraddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "useraddress":"'+useraddress+'" }';
                var code =  200;
                console.log("addauthorized: User has been given authorization rights");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error giving authorized user rights" }';
                var code =  403;
                console.log("addauthorized: Error:",error.reason);
                return[code,answer];

     })
}

async function deleteauthorized(useraddress) {
        if(useraddress == "" || typeof useraddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"User Address parameter is not properly defined" }';
                console.log("deleteauthorized: Error: User Address parameter is not properly defined");
                return[code,answer];
        }
        return await kykloscontract.methods.removeAuthorizedUser(useraddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "useraddress":"'+useraddress+'" }';
                var code =  200;
                console.log("deleteauthorized: User has been removed authorization rights");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error removing authorized user rights" }';
                var code =  403;
                console.log("deleteauthorized: Error:",error.reason);
                return[code,answer];
     })
}


async function isauthorized(useraddress) {
         if(useraddress == "" || typeof useraddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"User Address parameter is not properly defined" }';
                console.log("isauthorized: Error: User Address parameter is not properly defined");
                return[code,answer];
        }

        return await kykloscontract.methods.isAuthorized(useraddress).call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "isauthorized":"'+ans+'" }';
                var code =  200;
                console.log("isauthorized: Authorization rights have been checked");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error in checking authorized user rights" }';
                var code =  403;
                console.log("isauthorized: Error:",error.reason);
                return[code,answer];
     })
}

async function getauthorizednum() {
        return await kykloscontract.methods.getAuthorizedNum().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "ownernum":"'+ans+'" }';
                var code =  200;
                console.log("getauthorizednum: The number of authorized users have been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error un getting the number of authorized users" }';
                var code =  403;
                console.log("getauthorizednum: Error:",error.reason);
                return[code,answer];
     })
}


module.exports = {
        "addadmin": addadmin,
        "deleteadmin":deleteadmin,
        "isadmin": isadmin,
        "getadminnum": getadminnum,
        "getcomponents": getcomponents,
        "getcomponentsnum": getcomponentsnum,
        "addauthorized": addauthorized,
        "deleteauthorized": deleteauthorized,
        "isauthorized": isauthorized,
        "getauthorizednum": getauthorizednum
};
