#!/bin/sh

#
# [docker_build.sh]
# COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
# Licensed under LGPLv3.0 only license.
# SPDX-License-Identifier: LGPL-3.0-only.
#

echo "creating KYKLOS Blockchain Client docker image"
docker build -f Dockerfile -t kyklos_blockchain_client:latest .
