//
// [KYKLOS.sol]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//

pragma solidity ^0.5.0;
import './Component.sol';

contract KYKLOS {
    
    event newAdminEvent(address newAdmin, address addedBy, string rol);
    event removeAdminEvent(address admin, string rol);
    event newComponentEvent(address component, address owner, string organization, string name, string description, string deployment, uint256 creationTimestamp, uint256 updatedTimestamp);
   
    mapping(address => bool) public validAdmin;
    uint256 adminNum;
    
    mapping(address => bool) public validUsers;
    uint256 userNum;
    
    mapping(address => bool) public validComponents;
    uint256 componentNum;

    address[] private componentsAddress;
    mapping(address => address) componentsperowner;
    
    bool initialized;
 
    constructor()  public {
        validAdmin[tx.origin] = true;
        adminNum++;
        initialized=true;
    }
   
    function addAdmin(address _newAdmin) public onlyAdmin {
        if(initialized){
            initialized = false;
            emit newAdminEvent(tx.origin, tx.origin, "admin");
        }
        
        if(validAdmin[_newAdmin] != true){
            validAdmin[_newAdmin] = true;
            adminNum++;
            emit newAdminEvent(_newAdmin, tx.origin, "admin");
        }
    }
   
    function removeAdmin(address _admin) public onlyAdmin {
        require(adminNum>=1, "It is not possible to remove the admin; it is the only one");
        if(validAdmin[_admin] == true){
            validAdmin[_admin] = false;
            adminNum--;
            emit removeAdminEvent(_admin, "admin");
        }
    }
    
    function getAdminNum() public onlyAdmin view returns (uint256)  {
        return adminNum;
    }
    
    function isAdmin(address _admin) public onlyAdmin view returns (bool)  {
        if(validAdmin[_admin] == true) {
            return true;
        } else {
            return false;
        }
    }
    
    function getComponents() public onlyAdmin view returns (address[] memory)  {
        return componentsAddress;
    }
   
    function getComponentsNum() public onlyAdmin view returns (uint256)  {
        return componentNum;
    }
    
    function addAuthorizedUser(address _newUser) public onlyAdmin {
        if(validUsers[_newUser] != true){
            validUsers[_newUser] = true;
            userNum++;
            emit newAdminEvent(_newUser, tx.origin, "authorized_user");
        }
    }
   
    function removeAuthorizedUser(address _user) public onlyAdmin {
        if(validUsers[_user] == true){
            validUsers[_user] = false;
            userNum--;
            emit removeAdminEvent(_user, "authorized_user");
        }
    }
    
    function getAuthorizedNum() public onlyAdmin view returns (uint256)  {
        return userNum;
    }
    
    function isAuthorized(address _user) public onlyAdmin view returns (bool)  {
        if(validUsers[_user] == true) {
            return true;
        } else {
            return false;
        }
    }
    
   
    function createComponent (string memory _deployment, string memory _organization, string memory _name, string memory _description) public onlyAuthorizedUser returns (address) {
        
        if(validComponents[tx.origin] != true) {
            Component newComponent = new Component(tx.origin, _organization, _name, _description, _deployment, block.timestamp);
            
            address newComponentAddress = address(newComponent);
            componentsAddress.push(newComponentAddress);
            componentsperowner[tx.origin] = newComponentAddress;
            validComponents[tx.origin] = true;
            componentNum++;
            
            emit newComponentEvent(newComponentAddress, tx.origin, _organization, _name, _description, _deployment, block.timestamp, block.timestamp );
        }
    }
    
    function getcomponentbyowner() public view returns (address)  {
        return(componentsperowner[tx.origin]);
    }

   
    modifier onlyAdmin() {
        require(validAdmin[tx.origin], "This function can only be invoked from a valid admin account");
        _;
    }
    
    modifier onlyAuthorizedUser() {
        require(validUsers[tx.origin], "This function can only be invoked from a valid authrized account");
        _;
    }


}
