//
// [Component.sol]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//

pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;


contract Component {
   
    event ComponentUpdated(address id, address owner, string variable, string value, uint256 updatedTimestamp, string email);
    event NewComponentFeature(address id, address owner, string variableName, string variableValue, uint256 updatedTimestamp, string email);
    event NewValueFeature(address id, address owner, string variableName, string variableValue, uint256 updatedTimestamp, string email); 
   
    address id;
    address owner;
    string deployment;
    string organization;
    string name;
    string description;
    uint256 creationTimestamp;
    uint256 updatedTimestamp;
    string email;

    string[] features;
    mapping(string => string) featurelist;
    mapping(string => bool) featureValidity;
   
    constructor(address _owner, string memory _organization, string memory _name, string memory _description, string memory _deployment, uint256 _creationTimestamp) public {
        id=address(this);
        deployment = _deployment;
        owner = _owner;
        organization = _organization;
        name = _name;
        description = _description;
        creationTimestamp = _creationTimestamp;
        email = " ";
    }

    function addControlEmail(string memory _email) public onlyOwner {
        email=_email;
    }

    function deleteControlEmail() public onlyOwner {
        email=" ";
    }

    function setComponentOrganization (string memory _organization) public onlyOwner{
        organization = _organization;
        updatedTimestamp = block.timestamp;
        string memory variable = "organization";
        emit ComponentUpdated(id, tx.origin, variable, _organization, updatedTimestamp, email);
    }
    
    function setComponentDeployment (string memory _deployment) public onlyOwner{
        deployment = _deployment;
        updatedTimestamp = block.timestamp;
        string memory variable = "deployment";
        emit ComponentUpdated(id, tx.origin, variable, deployment, updatedTimestamp, email);
    }
    
    function setComponentName (string memory _name) public onlyOwner{
        name = _name;
        updatedTimestamp = block.timestamp;
        string memory variable = "name";
        emit ComponentUpdated(id, tx.origin, variable, name, updatedTimestamp, email);
    }
   
    function setComponentDescription (string memory _description) public onlyOwner{
        description = _description;
        updatedTimestamp = block.timestamp;
        string memory variable = "description";
        emit ComponentUpdated(id, tx.origin, variable, description, updatedTimestamp, email);
    }
   
    function setFeatureDescription (string memory _variableName, string memory _variableValue) public onlyOwner{
        featurelist[_variableName]=_variableValue;
        if(featureValidity[_variableName] != true) {
            features.push(_variableName);
            updatedTimestamp = block.timestamp;
            featureValidity[_variableName] = true;
            emit NewComponentFeature(id, tx.origin, _variableName, _variableValue, updatedTimestamp, email);
        } else {
            updatedTimestamp = block.timestamp;
            emit NewValueFeature(id, tx.origin, _variableName, _variableValue, updatedTimestamp, email);
        }
    }
   
    function getOwner() public view onlyOwner returns (address){
        return owner;
    }
    function getOrganization() public view onlyOwner returns (string memory){
        return(organization);
    }
    function getDeployment() public view onlyOwner returns (string memory){
        return(deployment);
    }
    function getName() public view onlyOwner returns (string memory){
        return(name);
    }
    function getDescription() public view onlyOwner returns (string memory){
        return(description);
    }
    function getId() public view onlyOwner returns (address){
        return(id);
    }
    
    function getCreationTime() public view onlyOwner returns (uint256){
        return(creationTimestamp);
    }
    
    function getUpdatedTime() public view onlyOwner returns (uint256){
        return(updatedTimestamp);
    }
   
    function getFeatureByValue(string memory _variableName) public view onlyOwner returns (string memory) {
        return featurelist[_variableName];
    }
    
    function getFeatureList() public view onlyOwner returns (string[] memory) {
        return features;
    }

    function getEmail() public view onlyOwner returns (string memory) {
        return email;
    }  
   
    modifier onlyOwner() {
        require(tx.origin == owner, "This function can only be invoked from a valid owner account");
        _;
    }
   

}
