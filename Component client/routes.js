//
// [routes.js]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//

var express = require('express');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
var http = require('http');
var functionality = require('./kyklos_bc.js');
var app = express();
const fs = require('fs');
const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};


app.post('/client/account', (req, res) => {
        [code,ans]=functionality.createaccount();
        res.status(code).send(JSON.parse(ans)) })

app.get('/client/account', (req, res) => {
        [code,ans]=functionality.getaccount(req.query.privatekey);
        res.status(code).send(JSON.parse(ans)) })

app.post('/client/wallet', (req, res) => {
        functionality.createwallet(req.query.privatekey).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/wallet', (req, res) => {
        [code,wallet]=functionality.getwallet();
        res.status(code).send(JSON.parse(wallet)) })

app.post('/client/registration', (req, res) => {
        functionality.sendregistrationrequest(req.query.address, req.query.email).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/subscription', (req, res) => {
        functionality.subscription(req.query.email).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/subscription', (req, res) => {
        [code,ans]=functionality.getsubscriptions();
        res.status(code).send(JSON.parse(ans)) })

app.delete('/client/subscription', (req, res) => {
        functionality.deletesubscription().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/admin', (req, res) => {
        functionality.isadmin(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/admin', (req, res) => {
        functionality.addadmin(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.delete('/client/admin', (req, res) => {
        functionality.deleteadmin(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/admins', (req, res) => {
        functionality.getadminnum().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/componentsnum', (req, res) => {
        functionality.getcomponentsnum().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/components', (req, res) => {
        functionality.getcomponents().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/authorizeduser', (req, res) => {
        functionality.isauthorized(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/authorizeduser', (req, res) => {
        functionality.addauthorized(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.delete('/client/authorizeduser', (req, res) => {
        functionality.deleteauthorized(req.query.address).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/authorizedusers', (req, res) => {
        functionality.getauthorizednum().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/component', (req, res) => {
        functionality.registercomponent(req.query.deployment, req.query.organization, req.query.name, req.query.description).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/organization', (req, res) => {
        functionality.getorganization().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/component/organization', (req, res) => {
        functionality.setorganization(req.query.organization).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/name', (req, res) => {
        functionality.getname().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/component/name', (req, res) => {
        functionality.setname(req.query.name).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/description', (req, res) => {
        functionality.getdescription().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/component/description', (req, res) => {
        functionality.setdescription(req.query.description).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/deployment', (req, res) => {
        functionality.getdeployment().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/component/deployment', (req, res) => {
        functionality.setdeployment(req.query.deployment).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.post('/client/component/feature', (req, res) => {
        functionality.setfeaturedescription(req.query.name, req.query.value).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/feature/:value', (req, res) => {
        functionality.getfeaturedescription(req.params.value).then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/features', (req, res) => {
        functionality.getfeatures().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/owner', (req, res) => {
        functionality.getowner().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/id', (req, res) => {
        functionality.getid().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/creationtime', (req, res) => {
        functionality.getcreationtime().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})

app.get('/client/component/updatedtime', (req, res) => {
        functionality.getupdatedtime().then(function([code,value]) {res.status(code).send(JSON.parse(value)) })})


http.createServer(options, app).listen(8001, () => {
        console.log("KYKLOS Client HTTP Server Started in the component..."); })
app.use('/doc',swaggerUi.serve, swaggerUi.setup(swaggerFile));
