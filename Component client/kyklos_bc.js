//
// [kyklos_bc.js]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//

const Web3 = require('web3')
const web3 = new Web3(new Web3.providers.HttpProvider('https://kyklos.bclab.dev/quorum'));
var kyklosabi = require('./kyklosabi.js');
var sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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')
const kyklosaddress = '0x1932c48b2bF8102Ba33B4A6B545C32236e342f34';
const kykloscontract = new web3.eth.Contract(kyklosabi.mainabi, kyklosaddress, {gasPrice:'0'});
var num_component = 0;
var kykloscomponentaddress ;
const index = 0;
const estimatedgas = 8000000;
var myaddress;
var kykloscomponentcontract;
web3.eth.handleRevert = true;

async function sendregistrationrequest(address, email) {
        if(address=="" || typeof address == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Address parameter is not properly defined" }';
                console.log("sendregistrationrequest: Error: Address parameter is not properly defined");
                return[code,answer];
        }

        if(email=="" || typeof email == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Email parameter is not properly defined" }';
                console.log("sendregistrationrequest: Error: Email parameter is not properly defined");
                return[code,answer];
        }

        const msg = {
                to: 'trustworthykyklos@gmail.com', // Change to your recipient
                from: 'trustworthykyklos@gmail.com', // Change to your verified sender
                subject: 'New registration in KYKLOS',
                text: 'KYKLOS registration request for address "'+address+'" from "'+email+'"',
        }

        try {
                sgMail.send(msg);
                var code = 200;
                var answer = '{"status":"OK", "address":"'+address+'" }';
                console.log("sendregistrationrequest: Request done");
                return[code,answer];
        } catch(error) {
                var code=403;
                var answer ='{"status":"Error", "error":"Error sending the email" }';
                console.log("sendregistrationrequest: Error: Error sending the email: ", error);
                return(code,answer);
        }
}

function createaccount() {
        var account=web3.eth.accounts.create();
        var answer = '{"status":"OK", "address":"'+account.address+'","privatekey":"'+account.privateKey+'" }';
        var code = 200;
        console.log("createaccount: Account created!");
        return[code,answer];
}

function getaccount(privatekey) {
        if(privatekey == "" || typeof privatekey == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Private key parameter is not properly defined" }';
                console.log("getaccount: error: Private key parameter is not properly defined");
                return[code,answer];
        }
        try {
                var account=web3.eth.accounts.privateKeyToAccount(privatekey);
                var answer = '{"status":"OK", "address":"'+account.address+'" }';
                var code = 200;
                console.log("getaccount: Account has been obtained");
                return[code,answer];
        } catch (error) {
                var code = 403;
                var answer ='{"status":"Error", "error":"Error getting account" }';
                console.log("getaccount: Error: ",error);
                return[code,answer];
        }
}

async function createwallet(privatekey) {
        if(privatekey=="" || typeof privatekey == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Private key parameter is not properly defined" }';
                console.log("createwallet: Error: Private key parameter is not properly defined");
                return[code,answer];
        }
        if(web3.eth.accounts.wallet.length<1) {
                try {
                        var wallet=web3.eth.accounts.wallet.add(privatekey);
                        myaddress= web3.eth.accounts.wallet[index].address;
                        return await createcomponentcontract().then(function(ans) {
                                var code = 200;
                                var answer = '{"status":"OK", "address":"'+myaddress+'" }';
                                console.log("createwallet: Address has been added to wallet");
                                return[code,answer];})
                } catch (error) {
                        var code=403;
                        var answer ='{"status":"Error", "error":"Error adding account to wallet" }';
                        console.log("createwallet: Error:",error);
                        return[code,answer];
                }
        } else {
                myaddress = web3.eth.accounts.wallet[index].address;
                var code=200;
                var answer = '{"status":"OK", "address":"'+myaddress+'" }';
                console.log("createwallet: Error: An account already exists");
                return[code,answer];
        }
}

function getwallet() {
        try {
                var code = 200;
                var address = web3.eth.accounts.wallet[index].address;
                var answer = '{"status":"OK", "address":"'+address+'" }';
                console.log("getwallet: Address in wallet has been obtained");
                return[code,answer];
        } catch (error) {
                var code= 403;
                var answer ='{"status":"Error", "error":"Error in getting account from wallet" }';
                console.log("getwallet: Error:",error);
                return[code,answer];
        }
}

async function addadmin(adminaddress) {
        if(adminaddress == "" || typeof adminaddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Admin Address parameter is not properly defined" }';
                console.log("addadmin: Error: Admin Address parameter is not properly defined");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("addadmin: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.addAdmin(adminaddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(txRaw) {
                var answer = '{"status":"OK", "address":"'+adminaddress+'" }';
                var code =  200;
                console.log("addadmin: Administrator rights have been given");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error giving administrator rights" }';
                var code =  403;
                console.log("addadmin: Error:",error);
                return[code,answer];
     })
}


async function subscription(email) {
        if(email == "" || typeof email == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Email parameter is not properly defined" }';
                console.log("subscription: Error: Email parameter is not properly defined");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("subscription: Error:",error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setdescription: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.addControlEmail(email).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(txRaw) {
                var answer = '{"status":"OK", "email":"'+email+'" }';
                var code =  200;
                console.log("subscription: Email has been subscribed");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error subscribing email" }';
                var code =  403;
                console.log("subscription: Error:",error);
                return[code,answer];
     })
}





async function deleteadmin(adminaddress) {
        if(adminaddress == "" || typeof adminaddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Admin Address parameter is not properly defined" }';
                console.log("deleteadmin: Error: Admin Address parameter is not properly defined");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("deleteadmin: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.removeAdmin(adminaddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(txRaw) {
                var answer = '{"status":"OK", "address":"'+adminaddress+'" }';
                var code =  200;
                console.log("deleteadmin: Administrator rights have been removed");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error removing administrator rights" }';
                console.log("deleteadmin: Error:",error);
                var code =  403;
                return[code,answer];

     })
}



async function deletesubscription() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("deletesubscription: Error:",error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setdescription: Error: Component is not registered yet");
                return[code,answer];
        }


        return await kykloscomponentcontract.methods.deleteControlEmail().send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(txRaw) {
                var answer = '{"status":"OK"}';
                var code =  200;
                console.log("deletesubscription: Email has been unsubscribed");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error unsubscribing email" }';
                console.log("deletesubscription: Error:",error);
                var code =  403;
                return[code,answer];

     })
}



async function isadmin(adminaddress) {
        if(adminaddress == "" || typeof adminaddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"Admin Address parameter is not properly defined" }';
                console.log("isadmin: Error: Admin Address parameter is not properly defined");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("isadmin: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.isAdmin(adminaddress).call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "isadmin":"'+ans+'" }';
                var code =  200;
                console.log("isadmin: Administrator rights have been checked");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error checking Administrator rights" }';
                var code =  403;
                console.log("isadmin: Error:",error);
                return[code,answer];
     })
}

async function getadminnum() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getadminnum: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.getAdminNum().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "adminnum":"'+ans+'" }';
                var code =  200;
                console.log("getadminnum: The number of administrators have been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting admin num" }';
                var code =  403;
                console.log("getadminnum: Error:",error.reason);
                return[code,answer];
     })
}


async function getcomponents() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getcomponents: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.getComponents().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "componentid":"'+ans+'" }';
                var code =  200;
                console.log("getcomponents: The components ids have been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting components" }';
                var code =  403;
                console.log("getcomponents: Error:",error.reason);
                return[code,answer];
     })
}



async function getsubscriptions() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getsubscriptions: Error:",error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setdescription: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getEmails().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "emails":"'+ans+'" }';
                var code =  200;
                console.log("getsubscriptions: The subscribed emails have been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting subscribed emails" }';
                var code =  403;
                console.log("getsubscriptions: Error:",error.reason);
                return[code,answer];
     })
}


async function getcomponentsnum() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getcomponentsnum: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.getComponentsNum().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "componentssnum":"'+ans+'" }';
                var code =  200;
                console.log("getcomponentsnum: The number of components has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting components num" }';
                var code =  403;
                console.log("getcomponentsnum: Error:",error.reason);
                return[code,answer];
     })
}


async function addauthorized(useraddress) {
        if(useraddress == "" || typeof useraddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"User Address parameter is not properly defined" }';
                console.log("addauthorized: Error: User Address parameter is not properly defined");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("addauthorized: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.addAuthorizedUser(useraddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "useraddress":"'+useraddress+'" }';
                var code =  200;
                console.log("addauthorized: User has been given authorization rights");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error giving authorized user rights" }';
                var code =  403;
                console.log("addauthorized: Error:",error);
                return[code,answer];

     })
}

async function deleteauthorized(useraddress) {
        if(useraddress == "" || typeof useraddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"User Address parameter is not properly defined" }';
                console.log("deleteauthorized: Error: User Address parameter is not properly defined");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("deleteauthorized: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.removeAuthorizedUser(useraddress).send({to:kyklosaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "useraddress":"'+useraddress+'" }';
                var code =  200;
                console.log("deleteauthorized: User has been removed authorization rights");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error removing authorized user rights" }';
                var code =  403;
                console.log("deleteauthorized: Error:",error);
                return[code,answer];
     })
}


async function isauthorized(useraddress) {
         if(useraddress == "" || typeof useraddress == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"User Address parameter is not properly defined" }';
                console.log("isauthorized: Error: User Address parameter is not properly defined");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("isauthorized: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.isAuthorized(useraddress).call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "isauthorized":"'+ans+'" }';
                var code =  200;
                console.log("isauthorized: Authorization rights have been checked");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error in checking authorized user rights" }';
                var code =  403;
                console.log("isauthorized: Error:",error);
                return[code,answer];
     })
}

async function getauthorizednum() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                 console.log("getauthorizednum: Error:",error);
                return[code,answer];
        }

        return await kykloscontract.methods.getAuthorizedNum().call({to:kyklosaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "ownernum":"'+ans+'" }';
                var code =  200;
                console.log("getauthorizednum: The number of authorized users have been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error un getting the number of authorized users" }';
                var code =  403;
                console.log("getauthorizednum: Error:",error.reason);
                return[code,answer];
     })
}

async function registercomponent(deployment, organization, name, description) {
        if(deployment == "" || typeof deployment == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"deployment parameter is not properly defined" }';
                console.log("registercomponent: Error: Missing parameter");
                return[code,answer];
        }
        if(organization == "" || typeof organization == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"organization parameter is not properly defined" }';
                console.log("registercomponent: Error: Missing parameter");
                return[code,answer];
        }
        if(name == "" || typeof name == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"name parameter is not properly defined" }';
                console.log("registercomponent: Error: Missing parameter");
                return[code,answer];
        }
        if(description == "" || typeof description == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"description parameter is not properly defined" }';
                console.log("registercomponent: Error: Missing parameter");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("registercomponent: Error:", error);
                return[code,answer];
        }

        return await kykloscontract.methods.createComponent(deployment, organization, name, description).send({to:kyklosaddress, from:myaddress,  gas:estimatedgas}).then(function(ans){
                return createcomponentcontract();})
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error registering the component"}';
                var code =  403;
                console.log("registercomponent: Error:", error.reason);
                return[code,answer];
        })
}


async function createcomponentcontract() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("registercomponent: Error:", error);
                return[code,answer];
        }
        return await kykloscontract.methods.getcomponentbyowner().call({to:kyklosaddress, from:myaddress}).then(function(address) {
                if(address == "0x0000000000000000000000000000000000000000") {
                        var answer = '{"status":"Error", "error":"Component is not registered yet" }';
                        var code =  403;
                        console.log("registercomponent: Error: Component is not registered yet");
                        return[code,answer];

                }
                if(kykloscomponentaddress != address) {
                        myaddress = web3.eth.accounts.wallet[index].address;
                        kykloscomponentaddress=address;
                        kykloscomponentcontract= new web3.eth.Contract(kyklosabi.componentabi, kykloscomponentaddress, {gasPrice:'0'});
                        var answer = '{"status":"OK", "componentid":"'+address+'" }';
                        var code =  200;
                        console.log("registercomponent: Component has been registered");
                        return[code,answer];
                } else {
                        var answer = '{"status":"OK", "componentid":"'+kykloscomponentaddress+'" }';
                        var code =  200;
                        console.log("registercomponent: Error: Component already registered");
                        return[code,answer];
                }
        })
}


async function setorganization(organization) {
        if(organization == "" || typeof organization == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"organization parameter is not properly defined" }';
                console.log("setorganization: Error: Missing parameter");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("setorganization: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setorganization: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.setComponentOrganization(organization).send({to:kykloscomponentaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "componentOrganization":"'+organization+'"}';
                var code =  200;
                console.log("setorganization: Organization has been added");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error providing organization" }';
                var code =  403;
                console.log("setorganization: Error:",error.reason);
                return[code,answer];
     })
}

async function setname(name) {
        if(name == "" || typeof name == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"name parameter is not properly defined" }';
                console.log("setname: Error: Missing parameter");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("setname: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setname: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.setComponentName(name).send({to:kykloscomponentaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "componentName":"'+name+'"}';
                var code =  200;
                console.log("setname: Name has been added");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error providing name" }';
                var code =  403;
                console.log("setname: Error:",error.reason);
                return[code,answer];
     })
}


async function setdescription(description) {
        if(description == "" || typeof description == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"description parameter is not properly defined" }';
                console.log("setdescription: Error: Missing parameter");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("setdescription: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setdescription: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.setComponentDescription(description).send({to:kykloscomponentaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "componentDescription":"'+description+'"}';
                var code =  200;
                console.log("setdescription: Description has been added");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error providing description" }';
                var code =  403;
                console.log("setdescription: Error:",error.reason);
                return[code,answer];
     })
}

async function setdeployment(deployment) {
        if(deployment == "" || typeof deployment == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"deployment parameter is not properly defined" }';
                console.log("setdeployment: Error: Missing parameter");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("setdeployment: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setdeployment: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.setComponentDeployment(deployment).send({to:kykloscomponentaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "componentDeployment":"'+deployment+'"}';
                var code =  200;
                console.log("setdeployment: Description has been added");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error providing deployment" }';
                var code =  403;
                console.log("setdeployment: Error:",error.reason);
                return[code,answer];
     })
}


async function setfeaturedescription(value, description) {
        if(value == "" || typeof value == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"value parameter is not properly defined" }';
                console.log("setfeaturedescription: Error: Missing parameter");
                return[code,answer];
        }
        if(description == "" || typeof description == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"description parameter is not properly defined" }';
                console.log("setfeaturedescription: Error: Missing parameter");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("setfeaturedescription: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("setfeaturedescription: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.setFeatureDescription(value, description).send({to:kykloscomponentaddress, from:myaddress, gas:estimatedgas}).then(function(ans) {
                var answer = '{"status":"OK", "componentFeatureName":"'+value+'", "componentFeatureValue":"'+description+'"}';
                var code =  200;
                console.log("setfeaturedescription: Feature has been added");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error providing a feature" }';
                var code =  403;
                console.log("setfeaturedescription: Error:",error.reason);
                return[code,answer];
     })
}


async function getorganization() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getorganization: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getorganization: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getOrganization().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "organization":"'+ans+'"}';
                var code =  200;
                console.log("getorganization: Component organization has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component organization" }';
                var code =  403;
                console.log("getorganization: Error:", error.reason);
                return[code,answer];
     })
}

async function getname() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getname: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getname: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getName().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "name":"'+ans+'"}';
                var code =  200;
                console.log("getname: Component name has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component name" }';
                var code =  403;
                console.log("getname: Error:", error.reason);
                return[code,answer];
     })
}


async function getdescription() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getdescription: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getdescription: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getDescription().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "description":"'+ans+'"}';
                var code =  200;
                console.log("getdescription: Component description has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component description" }';
                var code =  403;
                console.log("getdescription: Error:", error.reason);
                return[code,answer];
     })
}


async function getdeployment() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getdeploymentn: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getdeployment: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getDeployment().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "deployment":"'+ans+'"}';
                var code =  200;
                console.log("getdeployment: Component deployment has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component deployment" }';
                var code =  403;
                console.log("getdeployment: Error:", error.reason);
                return[code,answer];
     })
}

async function getowner() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getowner: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getowner: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getOwner().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "owner":"'+ans+'"}';
                var code =  200;
                console.log("getowner: Component owner has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component owner" }';
                var code =  403;
                console.log("getowner: Error:", error.reason);
                return[code,answer];
     })
}

async function getid() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getid: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getid: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getId().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "id":"'+ans+'"}';
                var code =  200;
                console.log("getid: Component id has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component id" }';
                var code =  403;
                console.log("getid: Error:", error.reason);
                return[code,answer];
     })
}


async function getcreationtime() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getcreationtime: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getcreationtime: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getCreationTime().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "creationTime":"'+ans+'"}';
                var code =  200;
                console.log("getcreationtime: Component creation time has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component creation time" }';
                var code =  403;
                console.log("getcreationtime: Error:", error.reason);
                return[code,answer];
     })
}

async function getupdatedtime() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getupdatedtime: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getupdatedtime: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getUpdatedTime().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "updatedTime":"'+ans+'"}';
                var code =  200;
                console.log("getupdatedtime: Component updated time has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the component updated time" }';
                var code =  403;
                console.log("getupdatedtime: Error:", error.reason);
                return[code,answer];
     })
}


async function getfeaturedescription(value) {
        if(value == "" || typeof value == 'undefined'){
                var code = 400;
                var answer ='{"status":"Error", "error":"value parameter is not properly defined" }';
                console.log("getfeaturedescription: Error: Missing parameter");
                return[code,answer];
        }
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getfeaturedescription: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getfeaturedescription: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getFeatureByValue(value).call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                if(ans== ""){
                        var answer = '{"status":"Error", "error":"Incorect feature name"}';
                        var code =  400;
                        console.log("getfeaturedescription: Error: Incorrect feature name");
                        return[code,answer];
                }
                else {
                        var answer = '{"status":"OK", "featureValue":"'+ans+'"}';
                        var code =  200;
                        console.log("getfeaturedescription: Feature has been obtained");
                        return[code,answer];
                }
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting the feature value" }';
                var code =  403;
                console.log("getfeaturedescription: Error:", error.reason);
                return[code,answer];
     })
}


async function getfeatures() {
        try {
                myaddress = web3.eth.accounts.wallet[index].address;
        } catch (error) {
                var answer ='{"status":"Error", "error":"Error in the wallet" }';
                var code =  409;
                console.log("getfeatures: Error:", error);
                return[code,answer];
        }

        if(typeof kykloscomponentcontract == 'undefined'){
                var answer ='{"status":"Error", "error":"Component is not registered yet" }';
                var code =  401;
                console.log("getfeatures: Error: Component is not registered yet");
                return[code,answer];
        }

        return await kykloscomponentcontract.methods.getFeatureList().call({to:kykloscomponentaddress, from:myaddress}).then(function(ans) {
                var answer = '{"status":"OK", "features":"'+ans+'"}';
                var code =  200;
                console.log("getfeatures: Features List has been obtained");
                return[code,answer];
        })
        .catch(function(error) {
                var answer ='{"status":"Error", "error":"Error getting features" }';
                var code =  403;
                console.log("getfeatures: Error:", error.reason);
                return[code,answer];
     })
}

module.exports = {
        "createaccount": createaccount,
        "getaccount": getaccount,
        "createwallet": createwallet,
        "getwallet": getwallet,
        "sendregistrationrequest": sendregistrationrequest,
        "addadmin": addadmin,
        "deleteadmin":deleteadmin,
        "isadmin": isadmin,
        "getadminnum": getadminnum,
        "getcomponents": getcomponents,
        "getcomponentsnum": getcomponentsnum,
        "addauthorized": addauthorized,
        "deleteauthorized": deleteauthorized,
        "isauthorized": isauthorized,
        "getauthorizednum": getauthorizednum,
        "registercomponent": registercomponent,
        "createcomponentcontract": createcomponentcontract,
        "getcreationtime": getcreationtime,
        "getupdatedtime": getupdatedtime,
        "getorganization": getorganization,
        "getdeployment": getdeployment,
        "getname": getname,
        "getdescription": getdescription,
        "getowner": getowner,
        "getid": getid,
        "setorganization": setorganization,
        "setdeployment": setdeployment,
        "setname": setname,
        "setdescription": setdescription,
        "setfeaturedescription": setfeaturedescription,
        "getfeaturedescription": getfeaturedescription,
        "getfeatures":getfeatures,
        "getsubscriptions":getsubscriptions,
        "subscription":subscription,
        "deletesubscription":deletesubscription
};
