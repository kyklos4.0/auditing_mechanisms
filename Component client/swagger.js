//
// [swagger.js]
// COPYRIGHT: FUNDACIÓN TECNALIA RESEARCH & INNOVATION, 2022
// Licensed under LGPLv3.0 only license.
// SPDX-License-Identifier: LGPL-3.0-only.
//


const swaggerAutogen = require('swagger-autogen')()

const outputFile = './swagger_output.json'
const endpointsFiles = ['./routes.js']

swaggerAutogen(outputFile, endpointsFiles).then(() => {
    require('./routes.js')
})
